/**
 * Created by abhinav on 10/10/17.
 */

// variable declarations.
var user = {
  pName : '',
  sign: ''
};

var signs  = ['X','O'];
var turn = '';
var computerSign = '';
var colIds = [0, 1, 2, 3, 4, 5, 6, 7, 8];
var playerIds = [];
var computerIds = [];
var result = '';
var lastTurn = '';

// Initialize player.
function init(e) {
  e.preventDefault();
  document.getElementById('userEntryMode').style.display = 'none';
  document.getElementById('playMode').style.display = 'block';
  var frm = document.getElementById('playerEntryForm');
  user.pName = frm.elements.namedItem('pName').value;
  user.sign = frm.elements.namedItem('sign').value;
  turn = frm.elements.namedItem('turn').value;
  for(var i in  signs) {
    if (user.sign != signs[i]) {
  computerSign = signs[i];
  break;
}
  }
  if(turn === 'computer') {
    playGame(null)
  }
}

function playGame(e) {
  if (colIds.length > 0) {
    if (turn === 'player') { // check for player's turn
      if (e.target.innerHTML === '') {
        e.target.innerHTML = user.sign;
        e.target.classList.add('userEntry');
        var colId = parseInt(e.target.id.split('_')[1]);
        var cellIndex = colIds.indexOf(colId);
        colIds.splice(cellIndex, 1);
        turn = 'computer';
        playerIds.push(colId);
        var isMatch = checkWin();
        if (isMatch === false) {
          playGame(null);
        }
      }
    }
    else if (turn === 'computer') { // check for computer's  turn
      var cIndex = Math.floor(Math.random() * colIds.length);
      var rand = colIds[cIndex];
      colIds.splice(cIndex, 1);
      e = document.getElementById('col_' + rand);
      if (e.innerHTML === '') {
        e.innerHTML = computerSign;
        e.classList.add('computerEntry');
        turn = 'player';
        computerIds.push(rand);
      }
      checkWin()
    }
  }
}

function checkWin() {
  var pairs = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
  var isMatch = false;
  for(var i in pairs) {
    var resultElement = document.getElementById('results');
    // check if the player is win or not.
    if(playerIds.indexOf(pairs[i][0])>-1 && playerIds.indexOf(pairs[i][1])>-1 && playerIds.indexOf(pairs[i][2])>-1) {
      result = user.pName + ' won, restart the game';
      resultElement.innerHTML = result;
      isMatch = true;
      document.getElementById('col_'+pairs[i][0]).classList.add('winningPair');
      document.getElementById('col_'+pairs[i][1]).classList.add('winningPair');
      document.getElementById('col_'+pairs[i][2]).classList.add('winningPair');
      endGame();
      break
    }
    // check if the computer is win or not.
    else if(computerIds.indexOf(pairs[i][0])>-1 && computerIds.indexOf(pairs[i][1])>-1 && computerIds.indexOf(pairs[i][2])>-1) {
      result = 'computer won, restart the game';
      resultElement.innerHTML = result;
      isMatch = true;
      document.getElementById('col_'+pairs[i][0]).classList.add('winningPair');
      document.getElementById('col_'+pairs[i][1]).classList.add('winningPair');
      document.getElementById('col_'+pairs[i][2]).classList.add('winningPair');
      endGame();
      break
    }
    // if the game will tie.
    else if (colIds.length < 1) {
      result = 'The Game is a tie.';
      resultElement.innerHTML = result;
      endGame();
    }
  }
  return isMatch
}

function endGame() {
  lastTurn = turn;
  turn = '';
}

// restart the game  for same user.
function restart() {
  for(var i=0; i<9; i++) {
    document.getElementById('col_'+i).innerHTML = '';
    document.getElementById('col_'+i).classList.remove('computerEntry', 'userEntry', 'winningPair');
  }
  colIds = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  playerIds = [];
  computerIds = [];
  result = '';
  var resultElement = document.getElementById('results');
  resultElement.innerHTML = result;

  if (lastTurn === 'player') {
    turn = 'computer';
    playGame(null)
  } else {
    turn = 'player'
  }
}

// End the game for current user.
function resetPlayer() {
  user = {
    pName : '',
    sign: ''
  };
  signs  = ['X','O'];
  turn = '';
  computerSign = '';
  restart();
  var x = document.getElementById('userEntryMode');
  var y = document.getElementById('playMode');
  document.getElementById('playerEntryForm').reset();
  x.style.display = 'block';
  y.style.display = 'none';
}